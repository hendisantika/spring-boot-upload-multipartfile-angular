# Spring Boot Upload Multipart AngularJS

I. Technologies

* Java 8

* Maven 3.6.1

* IntelliJ IDEA Ultimate 2018.2

* Spring Boot: 1.5.17.RELEASE version

* JQuery

* Bootstrap

II. Step to do:

* Create SpringBoot project

* Create Storage Service

* Implement upload controller

* Implement AngularJs App

* Create upload page

* Init Storage for File System

* Run and check results

Run this project by this command : `gradle clean bootRun`

### Screenshot

Home Page

![Home Page](img/home.png "Home Page")

Upload File

![Upload File](img/upload.png "Upload File")

Get Uploaded File / Download

![Get Uploaded File](img/get.png "Get Uploaded File")