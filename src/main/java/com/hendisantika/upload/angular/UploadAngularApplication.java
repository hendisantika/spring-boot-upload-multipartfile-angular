package com.hendisantika.upload.angular;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

@SpringBootApplication
public class UploadAngularApplication implements CommandLineRunner {
	@Resource
	StorageService storageService;


	public static void main(String[] args) {
		SpringApplication.run(UploadAngularApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		storageService.deleteAll();
		storageService.init();
	}
}
